// this is the variable your picker should change
var pickedColor = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  // your colour picker code goes here

  // display the currently "picked" colour in the bottom-right
  // this is just here for the template - you can display it however you like
  // see the FAQ for details
  fill(pickedColor);
  rect(width-100, height-100, 100, 100);
}
